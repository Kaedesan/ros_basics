#include "husky_highlevel_controller/HuskyHighlevelController.hpp"
#include <string>

namespace husky_highlevel_controller {
  HuskyHighlevelController::HuskyHighlevelController(ros::NodeHandle& nodeHandle): nodeHandle_(nodeHandle){
    if (!readParameters()) {
      ROS_ERROR("Could not read parameters.");
      ros::requestShutdown();
    }
    subscriber_ = nodeHandle_.subscribe(topicName_, queueSize_,&HuskyHighlevelController::topicCallback, this);

    //serviceServer_ = nodeHandle_.advertiseService("get_smallestDistance", &HuskyHighlevelController::serviceCallback, this);

    ROS_INFO("Successfully launched node.");

  }

  HuskyHighlevelController::~HuskyHighlevelController()
  {
  }

  bool HuskyHighlevelController::readParameters()
  {
    if (!nodeHandle_.getParam("topic_name", topicName_) && !nodeHandle_.getParam("queue_size", queueSize_) ) return false;
    return true;
  }

  void HuskyHighlevelController::topicCallback(const sensor_msgs::LaserScan& message){

    const std::vector<float> data = message.ranges;

    if(data.size() > 0){
      float smallestDistance = data[0];
      int size = data.size();

      for (int i=1; i<size; i++){
        if( data[i] < smallestDistance){
          smallestDistance = data[i];
        }
      }
      ROS_INFO("The smallest distance measurement is: [%f]",smallestDistance);
    }
    else{
      ROS_INFO("No informations in ranges!");
    }
  }

} /*namespace*/
