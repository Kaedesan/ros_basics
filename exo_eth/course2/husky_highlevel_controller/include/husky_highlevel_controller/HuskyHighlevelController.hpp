#pragma once
//#include "husky_highlevel_controller/Algorithm.hpp"

// ROS
#include <ros/ros.h>
#include <sensor_msgs/LaserScan.h>
#include <std_srvs/Trigger.h>

namespace husky_highlevel_controller {

  class HuskyHighlevelController {

   public:
    HuskyHighlevelController(ros::NodeHandle& nodeHandle);

    virtual ~HuskyHighlevelController();

   private:

    bool readParameters();

    void topicCallback(const sensor_msgs::LaserScan& message);

    //bool serviceCallback(std_srvs::Trigger::Request& request,std_srvs::Trigger::Response& response);

    ros::NodeHandle& nodeHandle_;

    ros::Subscriber subscriber_;

    std::string topicName_;

    int queueSize_;

    //ros::ServiceServer serviceServer_;

    //Algorithm algorithm_;
  };

} /* namespace */
