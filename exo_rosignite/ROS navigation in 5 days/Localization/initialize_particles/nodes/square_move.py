#!/usr/bin/env python

import sys
import rospy
import math
import time
from geometry_msgs.msg import PoseWithCovarianceStamped, PoseWithCovariance, Twist
from std_srvs.srv import Empty, EmptyRequest


class MoveHusky:


    def __init__(self):

        # Initialize publisher
        self.publisher_vel = rospy.Publisher('cmd_vel', Twist,queue_size=1)
        self.cmd_vel = Twist()

        # Initialize subscribers
        self.sub_pose = rospy.Subscriber("amcl_pose", PoseWithCovarianceStamped, self.sub_pose_callback)
        self.pose = PoseWithCovariance()

        # Initialize service client
        rospy.wait_for_service('global_localization')
        self.initiate_particles_service = rospy.ServiceProxy('global_localization', Empty)
        self.initiate_particles_request = EmptyRequest()

        # Other parameters
        self.loop_rate = rospy.Rate(10)
        self.ctrl_c = False
        rospy.on_shutdown(self.shutdownhook)


    def publish_vel(self, xl, za):
        self.cmd_vel.linear.x = xl
        self.cmd_vel.angular.z = za
        self.publisher_vel.publish(self.cmd_vel)
    
    
    def shutdownhook(self):
            
            # works better than the rospy.is_shut_down()
            self.stop_robot()
            self.ctrl_c = True

    def stop_robot(self):
        print "Stop of the robot requested"

        for i in range(int(10*1.5)):
            self.publish_vel(0.0,0.0)
            self.loop_rate.sleep()

    def move_straight(self):

        speed_linear = 0.4
        speed_angular = 0.0
        distance = 2 # size of a side in meter

        for i in range(int(10*(distance/speed_linear) +1)):
            self.publish_vel(speed_linear,speed_angular)
            self.loop_rate.sleep()

    def turn_right(self):
        speed_linear = 0.0
        speed_angular = -0.8

        for i in range(int(10*2.5+1)):
            self.publish_vel(speed_linear,speed_angular)
            self.loop_rate.sleep()


    def move_in_square(self):

        i = 0

        while i<4 and not self.ctrl_c:
            rospy.loginfo("////// Turning right")
            self.turn_right()
            self.stop_robot()

            rospy.loginfo("////// Moving straight")
            self.move_straight()
            self.stop_robot()

            i += 1

        self.stop_robot()
        rospy.loginfo("////// Movement finished")


    def call_initiate_particles_service(self):
        try:
            self.initiate_particles_service(self.initiate_particles_request)
            print "Particles have been intiated."
        except rospy.ServiceException, e:
            print "Service call failed: %s"%e

    def sub_pose_callback(self,data):
        self.pose = data.pose

    def calculate_covariance(self):
        
        rospy.loginfo("////// Calculating Covariance")
        cov_x = self.pose.covariance[0]
        cov_y = self.pose.covariance[7]
        cov_z = self.pose.covariance[35]
        rospy.loginfo("## Cov X: " + str(cov_x) + " ## Cov Y: " + str(cov_y) + " ## Cov Z: " + str(cov_z))
        cov = (cov_x+cov_y+cov_z)/3
        
        return cov




if __name__ == "__main__":

    rospy.init_node('move_in_sqaure_husky_node', anonymous=True)
    MoveMyHusky = MoveHusky()
    covariance = 1

    MoveMyHusky.call_initiate_particles_service()
    MoveMyHusky.move_in_square()
    covariance = MoveMyHusky.calculate_covariance()
    rospy.loginfo("######## Total Covariance: " + str(covariance))

    while covariance > 0.65:
        rospy.loginfo("######## Total Covariance is higher than 0.65. Repeating the process...")
        MoveMyHusky.call_initiate_particles_service()
        MoveMyHusky.move_in_square()
        covariance = MoveMyHusky.calculate_covariance()
        rospy.loginfo("######## Total Covariance: " + str(covariance))
        

    rospy.loginfo("######## Total Covariance is lower than 0.65. Robot correctly localized!")
    rospy.loginfo("######## Exiting...")
    
        