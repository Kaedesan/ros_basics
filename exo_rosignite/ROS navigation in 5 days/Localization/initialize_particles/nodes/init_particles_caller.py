#!/usr/bin/env python

import sys
import rospy
from std_srvs.srv import Empty, EmptyRequest

def call_global_localization():
    rospy.wait_for_service('global_localization')
    try:
        initiate_particles = rospy.ServiceProxy('global_localization', Empty)
        initiate_particles(EmptyRequest())
        print "Particles have been intiated."
    except rospy.ServiceException, e:
        print "Service call failed: %s"%e



if __name__ == "__main__":

    print "Client for global_localization has been launched."
    call_global_localization()
