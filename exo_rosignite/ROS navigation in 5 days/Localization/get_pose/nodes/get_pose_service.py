#!/usr/bin/env python


import rospy
from std_srvs.srv import Empty, EmptyResponse
from geometry_msgs.msg import PoseWithCovarianceStamped, Pose

robot_pose = Pose()

def sub_callback(data):
    global robot_pose
    robot_pose = data.pose.pose

def handle_get_pose(request):
    print "Robot pose"
    print robot_pose
    return EmptyResponse()


def get_pose_server():
    rospy.init_node('get_pose_server')
    service = rospy.Service('get_pose', Empty, handle_get_pose)
    pose_sub = rospy.Subscriber("amcl_pose", PoseWithCovarianceStamped, sub_callback)
    print "Ready to provide the pose."
    rospy.spin()


if __name__ == "__main__":

    get_pose_server()


