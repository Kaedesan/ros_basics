#! /usr/bin/env python

import rospy
import time
import actionlib
from move_base_msgs.msg import MoveBaseGoal, MoveBaseResult, MoveBaseAction, MoveBaseFeedback

def feedback_callback(feedback):
    
    print('[Feedback] Going to Goal Pose...')

def send_goal_client(px,py,oz,ow):
    
    i = 0

    client = actionlib.SimpleActionClient('/move_base', MoveBaseAction)

    client.wait_for_server()

    # Creates a goal to send to the action server.
    pose_goal = MoveBaseGoal()
    
    pose_goal.target_pose.header.frame_id = 'map'
    pose_goal.target_pose.pose.position.x = px
    pose_goal.target_pose.pose.position.y = py
    pose_goal.target_pose.pose.position.z = 0.0
    pose_goal.target_pose.pose.orientation.x = 0.0
    pose_goal.target_pose.pose.orientation.y = 0.0
    pose_goal.target_pose.pose.orientation.z = oz
    pose_goal.target_pose.pose.orientation.w = ow

    client.send_goal(pose_goal,feedback_cb=feedback_callback)

    while (not client.wait_for_result()):

        print('[Result] State: %d'%(client.get_state()))

    print('[Result] State: %d'%(client.get_state()))

if __name__ == '__main__':
    try:

        rospy.init_node('send_goal_client_node')
        while(True):
            send_goal_client(1.16, -4.76, 0.75, 0.66)
            send_goal_client(-1.5,0.13,0.4,0.5)
            send_goal_client(2.3,4.2,0.75,0.66)

    except rospy.ROSInterruptException:
        print("program interrupted before completion")
