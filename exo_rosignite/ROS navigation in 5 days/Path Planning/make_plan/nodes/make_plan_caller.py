#!/usr/bin/env python

import sys
import rospy
from nav_msgs.srv import GetPlan, GetPlanRequest

def call_make_plan():
    rospy.wait_for_service('/move_base/make_plan')
    try:
        make_plan_service = rospy.ServiceProxy('/move_base/make_plan', GetPlan)
        make_plan_request = GetPlanRequest()

        make_plan_request.start.header.frame_id = 'map'
        make_plan_request.start.pose.position.x = 0
        make_plan_request.start.pose.position.y = 0
        make_plan_request.start.pose.position.z = 0
        make_plan_request.start.pose.orientation.x = 0
        make_plan_request.start.pose.orientation.y = 0
        make_plan_request.start.pose.orientation.z = 0
        make_plan_request.start.pose.orientation.w = 0
        make_plan_request.goal.header.frame_id = 'map'
        make_plan_request.goal.pose.position.x = 1
        make_plan_request.goal.pose.position.y = 2
        make_plan_request.goal.pose.position.z = 0
        make_plan_request.goal.pose.orientation.x = 0
        make_plan_request.goal.pose.orientation.y = 0
        make_plan_request.goal.pose.orientation.z = 0
        make_plan_request.goal.pose.orientation.w = 0
        
        plan = make_plan_service(make_plan_request)
        print "Plan has been calculated."
        print plan
    except rospy.ServiceException, e:
        print "Service call failed: %s"%e



if __name__ == "__main__":
    rospy.init_node("make_plan_client_node")
    print "Client for make_plan has been launched."
    call_make_plan()