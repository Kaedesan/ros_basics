#!/usr/bin/env python

import sys
import rospy
from nav_msgs.srv import GetMap, GetMapRequest
from std_msgs.msg import Empty


def get_map():
    rospy.wait_for_service('static_map')
    try:
        map_call = rospy.ServiceProxy('static_map', GetMap)
        map = map_call(GetMapRequest())
        return (map.map.info.resolution,map.map.info.width, map.map.info.height )
    except rospy.ServiceException, e:
        print "Service call failed: %s"%e



if __name__ == "__main__":

    print "Map info: resolution = %s, width = %s and height = %s ."%get_map()
