#include "geometry_msgs/Twist.h"
#include "ros/ros.h"
#include "std_srvs/Empty.h"

ros::Publisher pub;
geometry_msgs::Twist vel_message;

bool move_callback(std_srvs::Empty::Request &req,
                   std_srvs::Empty::Response &res) {
  // res.some_variable = req.some_variable + req.other_variable;
  ROS_INFO("move_callback has been called.");
  vel_message.linear.x = 0.1;
  vel_message.angular.z = 0.1;
  pub.publish(vel_message);
  ROS_INFO("vel_message has been published."); // We print an string whenever
                                               // the Service gets called
  return true;
}

int main(int argc, char **argv) {
  ros::init(argc, argv, "bb8_move_in_circle_service_server");
  ros::NodeHandle nh;

  pub = nh.advertise<geometry_msgs::Twist>("cmd_vel", 1000);

  vel_message.linear.x = 0.0;
  vel_message.linear.y = 0.0;
  vel_message.linear.z = 0.0;
  vel_message.angular.x = 0.0;
  vel_message.angular.y = 0.0;
  vel_message.angular.z = 0.0;

  ros::ServiceServer my_service =
      nh.advertiseService("/move_bb8_in_circle", move_callback);

  ROS_INFO("Service launched.");
  ros::spin(); // mantain the service open.

  return 0;
}
