#include "ros/ros.h"
#include "std_srvs/Empty.h"

int main(int argc, char **argv) {
  ros::init(argc, argv,
            "bb8_move_in_square_service_client"); // Initialise a ROS node with
                                                  // the name service_client
  ros::NodeHandle nh;

  // Create the connection to the service /trajectory_by_name
  ros::service::waitForService("/move_bb8_in_circle");

  ros::ServiceClient move_bb8_in_circle_service =
      nh.serviceClient<std_srvs::Empty>("/move_bb8_in_circle");

  std_srvs::Empty srv; // Create an object of type TrajByName

  if (move_bb8_in_circle_service.call(srv)) // Send through the connection the
                                            // name of the trajectory to execute
  {
    ROS_INFO("Service sent."); // Print the result given by the service called
  } else {
    ROS_ERROR("Failed to call service /move_bb8_in_circle");
    return 1;
  }

  return 0;
}