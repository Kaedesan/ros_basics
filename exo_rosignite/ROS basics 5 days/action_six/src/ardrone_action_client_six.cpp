#include <actionlib/client/simple_action_client.h>
#include <ardrone_as/ArdroneAction.h> // Note: "Action" is appended
#include <geometry_msgs/Twist.h>
#include <ros/ros.h>
#include <std_msgs/Empty.h>

int nImage = 0; // Initialization of a global variable

void doneCb(const actionlib::SimpleClientGoalState &state,
            const ardrone_as::ArdroneResultConstPtr &result) {
  ROS_INFO("[State Result]: %s", state.toString().c_str());
  ROS_INFO("The Action has been completed");
  // ros::shutdown();
}

void activeCb() { ROS_INFO("Goal just went active"); }

void feedbackCb(const ardrone_as::ArdroneFeedbackConstPtr &feedback) {
  ROS_INFO("[Feedback] image n.%d received", nImage);
  ++nImage;
}

int main(int argc, char **argv) {

  ros::init(argc, argv,
            "drone_action_client_six"); // Initializes the action client node

  ros::NodeHandle nh;

  // Create the connection to the action server
  actionlib::SimpleActionClient<ardrone_as::ArdroneAction> client(
      "ardrone_action_server", true);
  client.waitForServer(); // Waits until the action server is up and running

  ros::Publisher vel_pub = nh.advertise<geometry_msgs::Twist>("cmd_vel", 100);
  geometry_msgs::Twist vel_msg;

  ros::Duration(3).sleep();

  ros::Publisher takeoff_pub =
      nh.advertise<std_msgs::Empty>("/drone/takeoff", 100);
  std_msgs::Empty takeoff_mess;

  ros::Duration(3).sleep();

  ros::Publisher land_pub = nh.advertise<std_msgs::Empty>("/drone/land", 100);
  std_msgs::Empty land_mess;

  ros::Duration(3).sleep();

  ardrone_as::ArdroneGoal goal; // Creates a goal to send to the action server
  goal.nseconds = 10;
  client.sendGoal(goal, &doneCb, &activeCb, &feedbackCb);

  ros::Rate loop_rate(1);
  actionlib::SimpleClientGoalState state_result = client.getState();
  ROS_INFO("[State Result]: %s", state_result.toString().c_str());

  takeoff_pub.publish(takeoff_mess);
  ROS_INFO("Taking off Drone...");
  ros::Duration(3).sleep();

  while (state_result == actionlib::SimpleClientGoalState::ACTIVE ||
         state_result == actionlib::SimpleClientGoalState::PENDING) {
    ROS_INFO("Moving Around");
    vel_msg.linear.x = 0.8;
    vel_msg.angular.z = 0.8;
    vel_pub.publish(vel_msg);
    loop_rate.sleep();
    state_result = client.getState();
    ROS_INFO("[State Result]: %s", state_result.toString().c_str());
  }
  vel_msg.linear.x = 0.0;
  vel_msg.angular.z = 0.0;
  vel_pub.publish(vel_msg);
  land_pub.publish(land_mess);
  ROS_INFO("Landing Drone ...");

  return 0;
}
