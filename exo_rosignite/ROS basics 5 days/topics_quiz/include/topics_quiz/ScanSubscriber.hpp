#pragma once

// ROS
#include "topics_quiz/CmdVelPublisher.hpp"
#include <ros/ros.h>
#include <sensor_msgs/LaserScan.h>

namespace topics_quiz {

class ScanSubscriber {

public:
  ScanSubscriber(ros::NodeHandle &nodeHandle,
                 topics_quiz::CmdVelPublisher &publisher);

  virtual ~ScanSubscriber();

private:
  void scanCallback(const sensor_msgs::LaserScan &message);

  ros::NodeHandle &nodeHandle_;

  ros::Subscriber subscriber_;

  topics_quiz::CmdVelPublisher publisher_;
};

} // namespace topics_quiz
