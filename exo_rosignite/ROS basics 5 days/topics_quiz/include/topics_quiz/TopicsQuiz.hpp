namespace topics_quiz {

class TopicsQuiz {

public:
  TopicsQuiz(ros::NodeHandle &nodeHandle, geometry_msgs::Twist *message);

  virtual ~TopicsQuiz();

  // void publishVel();

private:
  void scanCallback(const sensor_msgs::LaserScan &message);

  ros::NodeHandle &nodeHandle_;

  ros::Subscriber subscriber_;

  // ros::Publisher publisher_;

  geometry_msgs::Twist *message_;
};

} // namespace topics_quiz
