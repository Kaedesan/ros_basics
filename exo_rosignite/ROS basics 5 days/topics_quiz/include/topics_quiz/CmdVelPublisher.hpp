#pragma once

// ROS
#include <geometry_msgs/Twist.h>
#include <ros/ros.h>

namespace topics_quiz {

class CmdVelPublisher {

public:
  CmdVelPublisher(ros::NodeHandle &nodeHandle);

  virtual ~CmdVelPublisher();

  void topicPublish(float xl, float za);

private:
  ros::NodeHandle &nodeHandle_;

  ros::Publisher publisher_;

  geometry_msgs::Twist message_;
};

} // namespace topics_quiz
