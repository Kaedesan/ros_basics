#include "topics_quiz/ScanSubscriber.hpp"

namespace topics_quiz {
ScanSubscriber::ScanSubscriber(ros::NodeHandle &nodeHandle, topics_quiz::CmdVelPublisher& publisher)
    : nodeHandle_(nodeHandle), publisher_(publisher) {

  subscriber_ = nodeHandle_.subscribe("/kobuki/laser/scan", 100,
                                      &ScanSubscriber::scanCallback, this);

  ROS_INFO("Successfully launched subscriber.");

  // publisher_ = topics_quiz::CmdVelPublisher cmdVelPublisher(nodeHandle_);
}

ScanSubscriber::~ScanSubscriber() {}

void ScanSubscriber::scanCallback(const sensor_msgs::LaserScan &message) {

  const std::vector<float> data = message.ranges;

  if (data.size() > 0) {

    int size = data.size();
    float val_right, val_front, val_left;
    //float x_linear, z_angular;

    val_right = data[0];
    if (size % 2 == 0) {
      val_front = data[size / 2];
    } else {
      val_front = data[(size + 1) / 2];
    }
    val_left = data[size - 1];

    if ((val_front < 1) || (val_right < 1)) {
      publisher_.CmdVelPublisher::topicPublish(0.0, 0.5);
      //x_linear = 0.0;
      //z_angular = 0.5;
    } else if (val_left < 1) {
      publisher_.CmdVelPublisher::topicPublish(0.0, -0.5);
      //x_linear = 0.0;
      //z_angular = -0.5;
    } else {
      publisher_.CmdVelPublisher::topicPublish(0.5, 0.0);
      //x_linear = 0.5;
      //z_angular = 0.0;
    }

    //topics_quiz::CmdVelPublisher cmdVelPublisher(nodeHandle_, x_linear,
                                                // z_angular);

  } else {
    ROS_INFO("No informations in ranges!");
  }
}

} // namespace topics_quiz
