#include "topics_quiz/TopicsQuiz.hpp"

namespace topics_quiz {

TopicsQuiz::TopicsQuiz(ros::NodeHandle &nodeHandle,
                       geometry_msgs::Twist* message)
    : nodeHandle_(nodeHandle), message_(message) {

  // rate.expected_cycle_time_ = 1.0 / 2.0;

  // publisher_ = nodeHandle_.advertise<geometry_msgs::Twist>("cmd_vel", 100);
  // ROS_INFO("Successfully launched publisher.");

  subscriber_ = nodeHandle_.subscribe("/kobuki/laser/scan", 1000,
                                      &TopicsQuiz::scanCallback, this);
  ROS_INFO("Successfully launched subscriber.");

  message_->linear.x = 0.0;
  message_->linear.y = 0.0;
  message_->linear.z = 0.0;
  message_->angular.x = 0.0;
  message_->angular.y = 0.0;
  message_->angular.z = 0.0;
}

TopicsQuiz::~TopicsQuiz() {}

// void TopicsQuiz::publishVel(){
// publisher_.publish(message_);

// ROS_INFO("Successfully sent message.");
//}

void TopicsQuiz::scanCallback(const sensor_msgs::LaserScan &message) {

  const std::vector<float> data = message.ranges;

  if (data.size() > 0) {

    int size = data.size();
    float val_right, val_front, val_left;

    val_right = data[0];
    if (size % 2 == 0) {
      val_front = data[size / 2];
    } else {
      val_front = data[(size + 1) / 2];
    }
    val_left = data[size - 1];

    if ((val_front < 1) || (val_right < 1)) {
      message_->linear.x = 0.0;
      message_->angular.z = 0.5;
    } else if (val_left < 1) {
      message_->linear.x = 0.0;
      message_->angular.z = -0.5;
    } else {
      message_->linear.x = 0.5;
      message_->angular.z = 0.0;
    }
    //ROS_INFO("xl = [%f]", message_->linear.x);
    //ROS_INFO("za = [%f]", message_->angular.z);
    //ROS_INFO("Message has been modified!");
  } else {
    message_->linear.x = 0.0;
    message_->angular.z = 0.0;

    ROS_INFO("No informations in ranges!");
  }
}

} // namespace topics_quiz
