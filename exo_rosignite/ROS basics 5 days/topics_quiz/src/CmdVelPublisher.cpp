#include "topics_quiz/CmdVelPublisher.hpp"

namespace topics_quiz {
CmdVelPublisher::CmdVelPublisher(ros::NodeHandle &nodeHandle)
    : nodeHandle_(nodeHandle) {

  ros::Publisher publisher_ =
      nodeHandle_.advertise<geometry_msgs::Twist>("cmd_vel", 100);
  //ROS_INFO("Successfully launched publisher.");

  message_.linear.x = 0.0;
  message_.linear.y = 0.0;
  message_.linear.z = 0.0;
  message_.angular.x = 0.0;
  message_.angular.y = 0.0;
  message_.angular.z = 0.0;

  publisher_.publish(message_);
  ros::spinOnce();

  ROS_INFO("Successfully launched publisher.");
}

CmdVelPublisher::~CmdVelPublisher() {
  //
}

void CmdVelPublisher::topicPublish(float xl, float za) {

 message_.linear.x = xl;
 message_.angular.z = za;
 publisher_.publish(message_);
}
} // namespace topics_quiz
