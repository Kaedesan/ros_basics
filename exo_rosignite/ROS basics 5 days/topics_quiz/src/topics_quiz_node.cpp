#include "topics_quiz/TopicsQuiz.hpp"

//#include <ros/ros.h>

int main(int argc, char **argv) {

  ros::init(argc, argv, "topics_quiz_node");
  // ros::NodeHandle nodeHandle("~");
  ros::NodeHandle nodeHandle;
  geometry_msgs::Twist message;
  ros::Publisher velPublisher =
      nodeHandle.advertise<geometry_msgs::Twist>("cmd_vel", 1000);

  topics_quiz::TopicsQuiz topicsQuiz(nodeHandle, &message);

  ros::Rate r(2);

  while (ros::ok()) {
    // ROS_INFO("xl = [%f]", message.linear.x);
    // ROS_INFO("za = [%f]", message.angular.z);
    // topicsQuiz.publishVel();
    velPublisher.publish(message);
    ros::spinOnce();
    r.sleep();
  }

  // topics_quiz::CmdVelPublisher cmdVelPublisher(nodeHandle);

  // topics_quiz::ScanSubscriber scanSubscriber(nodeHandle, cmdVelPublisher);

  // ros::spin();
  return 0;
}
