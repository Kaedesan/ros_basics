#include "ros/ros.h"
#include <ros/package.h>
//#include "trajectory_by_name_srv/TrajByName.h"
// Import the service message used by the service /trajectory_by_name
#include "iri_wam_reproduce_trajectory/ExecTraj.h"

int main(int argc, char **argv) {

  ros::init(
      argc, argv,
      "my_robot_arm_demo"); // Initialise a ROS node with the name service_client
  ros::NodeHandle nh;

  // Create the connection to the service /trajectory_by_name
  ros::service::waitForService("/execute_trajectory");
  ros::ServiceClient execute_trajectory_service =
      nh.serviceClient<iri_wam_reproduce_trajectory::ExecTraj>(
          "/execute_trajectory");

  iri_wam_reproduce_trajectory::ExecTraj
      trajectory; // Create an object of type TrajByName

  trajectory.request.file =
      ros::package::getPath("iri_wam_reproduce_trajectory") +
      "/config/get_food.txt";

  if (execute_trajectory_service.call(trajectory)) {
    // Send through the connection the name of
    // the trajectory to execute

    ROS_INFO("Succeed to call service /execute_trajectory");
  } else {
    ROS_ERROR("Failed to call service /execute_trajectory");
    return 1;
  }

  return 0;
}