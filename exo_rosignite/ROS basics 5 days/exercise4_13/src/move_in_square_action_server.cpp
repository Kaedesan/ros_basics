#include <actionlib/TestAction.h>
#include <actionlib/server/simple_action_server.h>
#include <geometry_msgs/Twist.h>
#include <ros/ros.h>
#include <std_msgs/Empty.h>
#define M_PI 3.14159265358979323846 /* pi */

class SquareAction {
protected:
  ros::NodeHandle nh_;
  // NodeHandle instance must be created before this line. Otherwise strange
  // error occurs.
  actionlib::SimpleActionServer<actionlib::TestAction> as_;
  std::string action_name_;
  // create messages that are used to publish feedback and result
  actionlib::TestFeedback feedback_;
  actionlib::TestResult result_;
  ros::Publisher vel_pub;
  geometry_msgs::Twist vel_msg;
  ros::Publisher takeoff_pub;
  std_msgs::Empty takeoff_mess;
  ros::Publisher land_pub;
  std_msgs::Empty land_mess;

  void goStaight() {
    vel_msg.linear.x = 1.0;
    vel_msg.angular.z = 0.0;
    vel_pub.publish(vel_msg);
  }

  void goUp() { takeoff_pub.publish(takeoff_mess); }

  void goDown() {
    vel_msg.linear.x = 0.0;
    vel_msg.angular.z = 0.0;
    vel_pub.publish(vel_msg);
    land_pub.publish(land_mess);
  }

  void turnLeft() {
    vel_msg.linear.x = 0.0;
    vel_msg.angular.z = 0.50;
    vel_pub.publish(vel_msg);
    ros::Duration(3).sleep();
    // vel_msg.linear.x = 0.0;
    // vel_msg.angular.z = 0.0;
    // vel_pub.publish(vel_msg);
  }

public:
  SquareAction(std::string name)
      : as_(nh_, name, boost::bind(&SquareAction::executeCB, this, _1), false),
        action_name_(name) {
    as_.start();

    vel_pub = nh_.advertise<geometry_msgs::Twist>("cmd_vel", 100);
    ros::Duration(3).sleep();

    takeoff_pub = nh_.advertise<std_msgs::Empty>("/drone/takeoff", 100);
    ros::Duration(3).sleep();

    land_pub = nh_.advertise<std_msgs::Empty>("/drone/land", 100);
    ros::Duration(3).sleep();
  }

  ~SquareAction(void) {}

  void executeCB(const actionlib::TestGoalConstPtr &goal) {
    // helper variables
    ros::Rate r(1);
    bool success = true;

    // feedback_.feedback.clear();
    feedback_.feedback = 1; // first side of the square

    // publish info to the console for the user
    ROS_INFO("%s: Executing, moving in a square shape of size %i",
             action_name_.c_str(), goal->goal);

    // start executing the action
    for (int t = 1; t <= 3; t++) {
      // check that preempt has not been requested by the client
      if (as_.isPreemptRequested() || !ros::ok()) {
        ROS_INFO("%s: Preempted", action_name_.c_str());
        // set the action state to preempted
        as_.setPreempted();
        success = false;
        goDown();
        break;
      }
      goUp();
      r.sleep();
    }
    for (int c = 1; c <= 4; c++) {
      feedback_.feedback = c;

      for (int i = 1; i <= goal->goal; i++) {

        // check that preempt has not been requested by the client
        if (as_.isPreemptRequested() || !ros::ok()) {
          ROS_INFO("%s: Preempted", action_name_.c_str());
          // set the action state to preempted
          as_.setPreempted();
          success = false;
          goDown();
          break;
        }
        goStaight();
        // publish the feedback
        as_.publishFeedback(feedback_);
        r.sleep();
      }
      // check that preempt has not been requested by the client
      if (as_.isPreemptRequested() || !ros::ok()) {
        ROS_INFO("%s: Preempted", action_name_.c_str());
        // set the action state to preempted
        as_.setPreempted();
        success = false;
        goDown();
        break;
      }
      turnLeft();
      // publish the feedback
      as_.publishFeedback(feedback_);
      r.sleep();
    }

    if (success) {
      goDown();
      result_.result = 4 * goal->goal + 4 * 3;
      ROS_INFO("%s: Succeeded", action_name_.c_str());
      // set the action state to succeeded
      as_.setSucceeded(result_);
    }
  }
};

int main(int argc, char **argv) {
  ros::init(argc, argv, "move_in_square_action_server");

  SquareAction moveSquare("move_in_square");
  ros::spin();

  return 0;
}