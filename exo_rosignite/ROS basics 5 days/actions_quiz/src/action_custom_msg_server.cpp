#include "actions_quiz/CustomActionMsgAction.h"
#include <actionlib/server/simple_action_server.h>
#include <ros/ros.h>
#include <std_msgs/Empty.h>

class CustomAction {
protected:
  ros::NodeHandle nh_;

  actionlib::SimpleActionServer<actions_quiz::CustomActionMsgAction> as_;
  std::string action_name_;

  actions_quiz::CustomActionMsgFeedback feedback_;
  actions_quiz::CustomActionMsgResult result_;

  ros::Publisher takeoff_pub;
  std_msgs::Empty takeoff_mess;

  ros::Publisher land_pub;
  std_msgs::Empty land_mess;

  void takeoff_drone() { takeoff_pub.publish(takeoff_mess); }

  void land_drone() { land_pub.publish(land_mess); }

public:
  CustomAction(std::string name)
      : as_(nh_, name, boost::bind(&CustomAction::executeCB, this, _1), false),
        action_name_(name) {
    as_.start();

    takeoff_pub = nh_.advertise<std_msgs::Empty>("/drone/takeoff", 100);
    ros::Duration(4).sleep();

    land_pub = nh_.advertise<std_msgs::Empty>("/drone/land", 100);
    ros::Duration(4).sleep();
  }

  ~CustomAction(void) {}

  void executeCB(const actions_quiz::CustomActionMsgGoalConstPtr &goal) {

    // helper variables
    ros::Rate r(1);
    bool success = true;

    if (goal->goal == "TAKEOFF") {
      ROS_INFO("%s: Executing, the drone is taking off.", action_name_.c_str());
      feedback_.feedback = "The drone is taking off ...";
      for (int t = 1; t <= 3; t++) {
        // check that preempt has not been requested by the client
        if (as_.isPreemptRequested() || !ros::ok()) {
          ROS_INFO("%s: Preempted", action_name_.c_str());
          // set the action state to preempted
          as_.setPreempted();
          success = false;
          land_drone();
          break;
        }
        takeoff_drone();
        as_.publishFeedback(feedback_);
        r.sleep();
      }
    } else if (goal->goal == "LAND") {
      ROS_INFO("%s: Executing, the drone is landing.", action_name_.c_str());
      feedback_.feedback = "The drone is landing ...";

      for (int t = 1; t <= 3; t++) {
        // check that preempt has not been requested by the client
        if (as_.isPreemptRequested() || !ros::ok()) {
          ROS_INFO("%s: Preempted", action_name_.c_str());
          // set the action state to preempted
          as_.setPreempted();
          success = false;
          takeoff_drone();
          break;
        }
        land_drone();
        as_.publishFeedback(feedback_);
        r.sleep();
      }
    } else {
      ROS_INFO("%s: Executing, the drone is doing nothing.",
               action_name_.c_str());
      feedback_.feedback = "The drone is doing nothing ...";
      ROS_INFO("%s: Preempted", action_name_.c_str());
      // set the action state to preempted
      as_.setPreempted();
      as_.publishFeedback(feedback_);
      success = false;
    }

    if (success) {
      ROS_INFO("%s: Succeeded", action_name_.c_str());
      // set the action state to succeeded
      as_.setSucceeded(result_);
    }
  }
};

int main(int argc, char **argv) {
  ros::init(argc, argv, "action_custom_msg_server");

  CustomAction CustomAction("action_custom_msg_as");
  ros::spin();

  return 0;
}