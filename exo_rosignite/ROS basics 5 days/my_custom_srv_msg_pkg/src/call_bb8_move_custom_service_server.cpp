#include "ros/ros.h"
// Import the service message used by the service /move_bb8_in_circle_custom
#include "my_custom_srv_msg_pkg/MyCustomServiceMessage.h"

int main(int argc, char **argv) {

  // Initialise a ROS node
  ros::init(argc, argv, "call_bb8_move_custom_service_server");

  ros::NodeHandle nh;

  ros::service::waitForService("/move_bb8_in_circle_custom");

  ros::ServiceClient move_bb8_in_circle_custom_service =
      nh.serviceClient<my_custom_srv_msg_pkg::MyCustomServiceMessage>(
          "/move_bb8_in_circle_custom");

  my_custom_srv_msg_pkg::MyCustomServiceMessage
      srv; // Create an object of type MyCustomServiceMessage

  srv.request.duration = 9; // Fill the variable duration with the desired value

  if (move_bb8_in_circle_custom_service.call(srv) &&
      (srv.response.success == true)) {

    ROS_INFO("Service successfully called. Moving BB8 in a circle.");
  } else {
    ROS_ERROR("Failed to call service /move_bb8_in_circle_custom");
    return 1;
  }

  return 0;
}