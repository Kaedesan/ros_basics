#include "geometry_msgs/Twist.h"
#include "my_custom_srv_msg_pkg/MyCustomServiceMessage.h"
#include "ros/ros.h"

ros::Publisher pub;
geometry_msgs::Twist vel_message;

bool move_callback(
    my_custom_srv_msg_pkg::MyCustomServiceMessage::Request &req,
    my_custom_srv_msg_pkg::MyCustomServiceMessage::Response &res) {

  ROS_INFO("move_callback has been called.");
  ros::Rate r(1);

  int iteration = req.duration;

  vel_message.linear.x = 0.1;
  vel_message.angular.z = 0.1;

  while (iteration > 0) {
    pub.publish(vel_message);
    iteration--;
    r.sleep();
    ROS_INFO("vel_message has been published.");
  }
  vel_message.linear.x = 0.0;
  vel_message.angular.z = 0.0;
  pub.publish(vel_message);
  res.success = true;

  return true;
}

int main(int argc, char **argv) {
  ros::init(argc, argv, "bb8_move_custom_service_server");
  ros::NodeHandle nh;

  pub = nh.advertise<geometry_msgs::Twist>("cmd_vel", 1000);

  vel_message.linear.x = 0.0;
  vel_message.linear.y = 0.0;
  vel_message.linear.z = 0.0;
  vel_message.angular.x = 0.0;
  vel_message.angular.y = 0.0;
  vel_message.angular.z = 0.0;

  ros::ServiceServer my_service =
      nh.advertiseService("/move_bb8_in_circle_custom", move_callback);

  ROS_INFO("Service launched.");
  ros::spin(); // mantain the service open.

  return 0;
}