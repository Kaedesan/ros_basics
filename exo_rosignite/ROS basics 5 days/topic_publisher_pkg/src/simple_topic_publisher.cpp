#include <geometry_msgs/Twist.h>
#include <ros/ros.h>
#include <std_msgs/Int32.h>

int main(int argc, char **argv) {

  ros::init(argc, argv, "topic_publisher");
  ros::NodeHandle nh;

  ros::Publisher pub = nh.advertise<geometry_msgs::Twist>("cmd_vel", 1000);
  ros::Rate loop_rate(2);

  geometry_msgs::Twist info;
  info.linear.x = 0.5;
  info.linear.y = 0.0;
  info.linear.z = 0.0;
  info.angular.x = 0.0;
  info.angular.y = 0.0;
  info.angular.z = 0.1;

  while (ros::ok()) {
    pub.publish(info);
    ros::spinOnce();
    loop_rate.sleep();
  }

  return 0;
}