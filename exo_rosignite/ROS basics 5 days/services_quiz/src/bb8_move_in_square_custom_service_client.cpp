#include "ros/ros.h"
// Import the service message used by the service /move_bb8_in_square_custom
#include "services_quiz/BB8CustomServiceMessage.h"

int main(int argc, char **argv) {

  // Initialise a ROS node
  ros::init(argc, argv, "b8_move_custom_service_client");

  ros::NodeHandle nh;

  ros::service::waitForService("/move_bb8_in_square_custom");

  ros::ServiceClient move_bb8_in_square_custom_service =
      nh.serviceClient<services_quiz::BB8CustomServiceMessage>(
          "/move_bb8_in_square_custom");

  services_quiz::BB8CustomServiceMessage srv_little_square;
  srv_little_square.request.side = 2;
  srv_little_square.request.repetitions = 2;

  services_quiz::BB8CustomServiceMessage srv_big_square;
  srv_big_square.request.side = 4;
  srv_big_square.request.repetitions = 1;

  if (move_bb8_in_square_custom_service.call(srv_little_square) &&
      (srv_little_square.response.success == true)) {

    ROS_INFO("Service successfully called. Moving BB8 in a small square.");
  } else {
    ROS_ERROR("Failed to call service /move_bb8_in_square_custom");
    return 1;
  }

  if (move_bb8_in_square_custom_service.call(srv_big_square) &&
      (srv_big_square.response.success == true)) {

    ROS_INFO("Service successfully called. Moving BB8 in a big square.");
  } else {
    ROS_ERROR("Failed to call service /move_bb8_in_square_custom");
    return 1;
  }

  return 0;
}