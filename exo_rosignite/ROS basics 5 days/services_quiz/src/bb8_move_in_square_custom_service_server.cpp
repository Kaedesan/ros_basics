#include "geometry_msgs/Twist.h"
#include "ros/ros.h"
#include "services_quiz/BB8CustomServiceMessage.h"
#define M_PI 3.14159265358979323846 /* pi */

ros::Publisher vel_publisher;
geometry_msgs::Twist vel_message;

bool move_callback(services_quiz::BB8CustomServiceMessage::Request &req,
                   services_quiz::BB8CustomServiceMessage::Response &res) {

  ROS_INFO("move_callback has been called.");

  float speed = 0.2;

  double distance_straight = 1.0 * req.side;
  int iteration = req.repetitions;
  ros::Rate r(10);

  while (iteration > 0) {

    for (int i = 1; i < 5; i++) {
      // side of the square
      vel_message.linear.x = speed;
      vel_message.angular.z = 0.0;
      vel_publisher.publish(vel_message);
      ros::Duration(distance_straight / speed).sleep();

      // turn
      vel_message.linear.x = 0.0;
      vel_message.angular.z = -speed;
      vel_publisher.publish(vel_message);
      ros::Duration((M_PI / 8) / speed).sleep();
    }
    iteration--;
    r.sleep();
  }
  vel_message.linear.x = 0.0;
  vel_message.angular.z = 0.0;
  vel_publisher.publish(vel_message);
  res.success = true;
  ROS_INFO("vel_message has been published.");
  return true;
}

int main(int argc, char **argv) {
  ros::init(argc, argv, "bb8_move_custom_service_server");
  ros::NodeHandle nh;

  vel_publisher = nh.advertise<geometry_msgs::Twist>("cmd_vel", 1000);

  vel_message.linear.x = 0.0;
  vel_message.linear.y = 0.0;
  vel_message.linear.z = 0.0;
  vel_message.angular.x = 0.0;
  vel_message.angular.y = 0.0;
  vel_message.angular.z = 0.0;

  ros::ServiceServer my_service =
      nh.advertiseService("/move_bb8_in_square_custom", move_callback);

  ROS_INFO("Service launched.");
  ros::spin(); // mantain the service open.

  return 0;
}