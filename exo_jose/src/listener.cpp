#include "ros/ros.h"
#include "std_msgs/String.h"


void messageCallback(const std_msgs::String::ConstPtr& msg)
{
  ROS_INFO("Jose send you: [%s]", msg->data.c_str());
}

int main(int argc, char **argv)
{

  ros::init(argc, argv, "listener");

  ros::NodeHandle n;

  ros::Subscriber sub = n.subscribe("message_for_alex", 1000, messageCallback);

  ros::spin();

  return 0;
}
